package com.example.ramin.sematec3.MVPB;

public class interface MVPContract {

    interface View {
        void onDataReceived(FlagModel result) ;
        void onConnectionFailed() ;
        void showLoading(boolean show) ;
    }

    interface Presenter {
        void attachView(View view) ;
        void orderToGetData() ;
        void onDataReceived(FlagModel result) ;
        void onConnectionFailed() ;
        void isOnLoading(boolean is) ;
    }

    interface Model {
        void attachPresenter(Presenter presenter) ;
        void orderToGetData() ;
    }
    
}
