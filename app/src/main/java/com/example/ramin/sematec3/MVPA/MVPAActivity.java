package MVPA;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.example.ramin.sematec3.R;

public class MVPAActivity extends AppCompatActivity implements MVPA.MVPContract.View {

        TextView result;
        EditText name;

        MVPA.MVPContract.Presenter presenter = (MVPA.MVPContract.Presenter) new MVPA.MVPPresenter();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mvpa);


        presenter.attachView(this);
        
        result = findViewById(R.id.result);
        name = findViewById(R.id.name);


        findViewById(R.id.getAge).setOnClickListener(V -> {
            presenter.onGetAge(name.getText().toString());

        });
        
    }

    @Override
    public void onAgeReceived(int age) {
        result.setText("age is " + age);
    }
}
