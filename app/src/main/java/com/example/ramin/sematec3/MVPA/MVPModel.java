package MVPA;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.ramin.sematec3.R;

public class MVPModel extends AppCompatActivity implements MVPA.MVPContract.Model {
    
    MVPA.MVPContract.Presenter presenter ;
 

    @Override
    public void attachPresenter(MVPA.MVPContract.Presenter presenter) {

        this.presenter = presenter;

    }

    @Override
    public void onGetAge(String name) {
        int age = 0 ;
        if(name.equals("alireza"))
            age = 20;
        else if(name.equals("mohammad"))
            age = 30;
        else
            age = 50 ;
        
        presenter.onAgeReceived(age);
    }
}
