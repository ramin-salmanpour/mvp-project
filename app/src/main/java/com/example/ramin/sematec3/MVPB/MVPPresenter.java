package com.example.ramin.sematec3.MVPB;

public class MVPPresenter implements MVPContract.Presenter {
    @Override
    public void attachView(MVPContract.View view) {

    }

    @Override
    public void orderToGetData() {

    }

    @Override
    public void onDataReceived(FlagModel result) {

    }

    @Override
    public void onConnectionFailed() {

    }

    @Override
    public void isOnLoading(boolean is) {

    }
}
