package MVPA;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.ramin.sematec3.R;

public interface MVPContract {

    interface  View{
        void onAgeReceived(int age) ;
    }
    interface Presenter{
        void attachView(View view) ;
        void onGetAge(String name) ;
        void onAgeReceived(int age) ;
    }

    interface Model{
        void attachPresenter(Presenter presenter) ;
        void onGetAge(String name) ;
    }
}

// MVPContract
