package MVPA;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.ramin.sematec3.R;

public class MVPPresenter extends AppCompatActivity implements MVPA.MVPContract.Presenter {

    MVPA.MVPContract.Model model = new MVPA.MVPModel() ;
    MVPA.MVPContract.View view ;

//    @Override
//    public void attachView(MVPContract.View view) {
//        this.view = view;
//        model.attachPresenter(this);
//    }

    @Override
    public void attachView(MVPA.MVPContract.View view) {
        this.view = view;
        model.attachPresenter(this);
    }

    @Override
    public void onGetAge(String name) {
        model.onGetAge(name);
    }

    @Override
    public void onAgeReceived(int age) {
        view.onAgeReceived(age);
    }
}
