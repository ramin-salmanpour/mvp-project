package com.example.ramin.sematec3.MVPB;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ramin.sematec3.MainActivity;
import com.example.ramin.sematec3.R;


public class MVPBActivity extends MainActivity
        implements View.OnClickListener
        , MVPContract.View {
    ImageView flag;
    TextView result;
    

    MVPPresenter presenter = new MVPPresenter() ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mvpb);
        bind();
        presenter.attachView(this);
    }

    void bind() {
        flag = findViewById(R.id.flag);
        result = findViewById(R.id.result);
        findViewById(R.id.reNew).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        presenter.orderToGetData();
    }


    @Override
    public void onDataReceived(FlagModel response) {
        flag.load(response.getFlag());
        result.append(response.getIp());
        result.append("\n");
        result.append(response.getCountryName());
        result.append("\n");
        result.append(response.getOrganisation());
    }

    @Override
    public void onConnectionFailed() {
        PublicMethods.Companion.toast(this
                , "error in connecting");
    }

    @Override
    public void showLoading(boolean show) {
        if(show)
            dialog.show();
        else
            dialog.dismiss();
    }
}